package in.glivade.museum;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.glivade.museum.app.AppController;
import in.glivade.museum.app.MyPreference;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.glivade.museum.app.Api.KEY_ERROR;
import static in.glivade.museum.app.Api.KEY_ID;
import static in.glivade.museum.app.Api.KEY_MESSAGE;
import static in.glivade.museum.app.Api.KEY_MOBILE;
import static in.glivade.museum.app.Api.KEY_NAME;
import static in.glivade.museum.app.Api.KEY_PASS;
import static in.glivade.museum.app.Api.KEY_TAG;
import static in.glivade.museum.app.Api.URL;
import static in.glivade.museum.app.Api.VALUE_REGISTER;
import static in.glivade.museum.app.MyActivity.launchClearStack;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private EditText editTextName, editTextMobile, editTextPass;
    private Button buttonRegister;
    private TextView textViewBackLogin;
    private AnimationDrawable animationDrawable;
    private MyPreference preference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            animationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonRegister) {
            processRegister();
        } else if (view == textViewBackLogin) {
            onBackPressed();
        }
    }

    private void initObjects() {
        editTextName = findViewById(R.id.input_name);
        editTextMobile = findViewById(R.id.input_mobile);
        editTextPass = findViewById(R.id.input_pass);
        buttonRegister = findViewById(R.id.btn_register);
        textViewBackLogin = findViewById(R.id.txt_back_login);

        context = this;
        animationDrawable = (AnimationDrawable) findViewById(R.id.register).getBackground();
        preference = new MyPreference(context);
        progressDialog = new ProgressDialog(context);
    }

    private void initCallbacks() {
        buttonRegister.setOnClickListener(this);
        textViewBackLogin.setOnClickListener(this);
    }

    private void processRegister() {
        String name = editTextName.getText().toString().trim();
        String mobile = editTextMobile.getText().toString().trim();
        String pass = editTextPass.getText().toString().trim();
        if (validateInput(name, mobile, pass)) {
            showProgressDialog();
            registerUser(name, mobile, pass);
        }
    }

    private boolean validateInput(String name, String mobile, String pass) {
        if (TextUtils.isEmpty(name)) {
            editTextName.requestFocus();
            editTextName.setError(getString(R.string.error_empty));
            return false;
        } else if (TextUtils.isEmpty(mobile)) {
            editTextMobile.requestFocus();
            editTextMobile.setError(getString(R.string.error_empty));
            return false;
        } else if (mobile.length() < 10) {
            editTextMobile.requestFocus();
            editTextMobile.setError(String.format(Locale.getDefault(),
                    getString(R.string.error_length), "Mobile No", 10, "digits"));
            return false;
        } else if (pass.isEmpty()) {
            editTextPass.requestFocus();
            editTextPass.setError(getString(R.string.error_empty));
            return false;
        } else if (pass.length() < 6) {
            editTextPass.requestFocus();
            editTextPass.setError(getString(R.string.error_pass_length));
            return false;
        }
        return true;
    }

    private void registerUser(final String name, final String mobile, final String pass) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleRegisterResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_REGISTER);
                params.put(KEY_NAME, name);
                params.put(KEY_MOBILE, mobile);
                params.put(KEY_PASS, pass);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "register");
    }

    private void handleRegisterResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt(KEY_ERROR);
            String message = jsonObject.getString(KEY_MESSAGE);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

            if (error == 0) {
                preference.setId(jsonObject.getInt(KEY_ID));
                preference.setName(jsonObject.getString(KEY_NAME));
                launchClearStack(context, MainActivity.class);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        progressDialog.setMessage("Creating account..");
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
