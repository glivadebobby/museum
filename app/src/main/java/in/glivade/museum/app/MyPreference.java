package in.glivade.museum.app;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

public class MyPreference {

    private static final String PREF_USER = "user";
    private static final String ID = "id";
    private static final String NAME = "name";
    private SharedPreferences mPreferencesUser;

    public MyPreference(Context context) {
        mPreferencesUser = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
    }

    public int getId() {
        return mPreferencesUser.getInt(encode(ID), -1);
    }

    public void setId(int id) {
        mPreferencesUser.edit().putInt(encode(ID), id).apply();
    }

    public String getName() {
        return mPreferencesUser.getString(encode(NAME), null);
    }

    public void setName(String name) {
        mPreferencesUser.edit().putString(encode(NAME), name).apply();
    }

    public void clearUser() {
        mPreferencesUser.edit().clear().apply();
    }

    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }
}
