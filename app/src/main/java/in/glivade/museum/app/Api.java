package in.glivade.museum.app;


public class Api {
    /**
     * Api Keys
     */
    public static final String KEY_TAG = "tag";
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_PASS = "password";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_ERROR = "error";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_QR_CODE = "qr_code";
    public static final String KEY_TITLE = "title";
    public static final String KEY_SHORT_DESCRIPTION = "short_description";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_AUTHORS = "authors";
    public static final String KEY_LAST_UPDATED = "last_updated";
    public static final String KEY_IMAGES = "images";
    public static final String KEY_VIDEOS = "videos";
    public static final String KEY_REVIEWS = "reviews";
    public static final String KEY_ARTICLE_ID = "article_id";
    public static final String KEY_URL = "url";
    public static final String KEY_CODE = "code";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_RATING = "rating";
    public static final String KEY_REVIEW = "review";
    /**
     * Api Values
     */
    public static final String VALUE_LOGIN = "login";
    public static final String VALUE_REGISTER = "register";
    public static final String VALUE_SCAN_RESULT = "scan_result";
    public static final String VALUE_ARTICLE = "article";
    public static final String VALUE_ADD_REVIEW = "add_review";
    public static final String VALUE_EDIT_REVIEW = "edit_review";
    /**
     * Api URLS
     */
    public static final String URL = "http://museum.glivade.in/api.php";
}
