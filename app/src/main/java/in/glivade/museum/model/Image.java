package in.glivade.museum.model;


public class Image {

    private int id, articleId;
    private String url;

    public Image(int id, int articleId, String url) {
        this.id = id;
        this.articleId = articleId;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
