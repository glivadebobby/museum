package in.glivade.museum.model;


public class Video {

    private int id, articleId;
    private String code;

    public Video(int id, int articleId, String code) {
        this.id = id;
        this.articleId = articleId;
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
