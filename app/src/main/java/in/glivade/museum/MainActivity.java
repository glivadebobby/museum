package in.glivade.museum;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import in.glivade.museum.app.AppController;
import in.glivade.museum.app.MyPreference;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.Manifest.permission.CAMERA;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static in.glivade.museum.app.Api.KEY_ERROR;
import static in.glivade.museum.app.Api.KEY_ID;
import static in.glivade.museum.app.Api.KEY_QR_CODE;
import static in.glivade.museum.app.Api.KEY_SHORT_DESCRIPTION;
import static in.glivade.museum.app.Api.KEY_TAG;
import static in.glivade.museum.app.Api.KEY_TITLE;
import static in.glivade.museum.app.Api.URL;
import static in.glivade.museum.app.Api.VALUE_SCAN_RESULT;
import static in.glivade.museum.app.MyActivity.launchClearStack;
import static in.glivade.museum.app.MyActivity.launchWithBundle;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        SurfaceHolder.Callback, Detector.Processor<TextBlock> {

    private static final int REQUEST_CAMERA = 1;
    private Toolbar toolbar;
    private SurfaceView surfaceView;
    private CardView cardViewScanResult;
    private LinearLayout layoutResult;
    private TextView textViewNoResult, textViewTitle, textViewShortDescription;
    private Button buttonReadMore;
    private AnimationDrawable mAnimationDrawable;
    private CameraSource cameraSource;
    private TextRecognizer textRecognizer;
    private MyPreference preference;
    private ProgressDialog progressDialog;
    private int articleId;
    private String mQrCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initToolbar();
        initCallbacks();
        processTextRecognizer();
        buildCameraSource();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cameraSource.release();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            preference.clearUser();
            launchClearStack(this, SplashActivity.class);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED)
                    startPreview();
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == buttonReadMore) {
            Bundle bundle = new Bundle();
            bundle.putInt(KEY_ID, articleId);
            launchWithBundle(this, ArticleActivity.class, bundle);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        cameraSource.stop();
    }

    @Override
    public void receiveDetections(Detector.Detections<TextBlock> detections) {
        SparseArray<TextBlock> items = detections.getDetectedItems();
        if (items.size() > 0) {
            final TextBlock item = items.valueAt(0);
            if (item != null && item.getValue() != null && (mQrCode == null
                    || !mQrCode.equals(item.getValue())) && item.getValue().length() == 8) {
                mQrCode = item.getValue();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getScanResult();
                    }
                });
            }
        }
    }

    @Override
    public void release() {

    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        surfaceView = findViewById(R.id.ocr);
        cardViewScanResult = findViewById(R.id.scan_result);
        layoutResult = findViewById(R.id.result);
        textViewNoResult = findViewById(R.id.txt_no_result);
        textViewTitle = findViewById(R.id.txt_title);
        textViewShortDescription = findViewById(R.id.txt_short_description);
        buttonReadMore = findViewById(R.id.btn_read_more);

        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.main).getBackground();
        textRecognizer = new TextRecognizer.Builder(this).build();
        preference = new MyPreference(this);
        progressDialog = new ProgressDialog(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initCallbacks() {
        buttonReadMore.setOnClickListener(this);
    }

    private void processTextRecognizer() {
        if (!textRecognizer.isOperational()) {
            Toast.makeText(this,
                    "Text detector dependencies are not available", Toast.LENGTH_SHORT).show();
        }
        textRecognizer.setProcessor(this);
    }

    private void buildCameraSource() {
        cameraSource = new CameraSource.Builder(this, textRecognizer)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setAutoFocusEnabled(true)
                .build();
        surfaceView.getHolder().addCallback(this);
    }

    @SuppressLint("MissingPermission")
    private void startPreview() {
        if (hasCameraPermission()) {
            try {
                cameraSource.start(surfaceView.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else requestCameraPermission();
    }

    private void getScanResult() {
        showProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleScanResult(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                Toast.makeText(MainActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_SCAN_RESULT);
                params.put(KEY_QR_CODE, mQrCode);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "scan-result");
    }

    private void handleScanResult(String response) {
        cardViewScanResult.setVisibility(View.VISIBLE);
        try {
            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt(KEY_ERROR);

            if (error == 0) {
                layoutResult.setVisibility(View.VISIBLE);
                textViewNoResult.setVisibility(View.GONE);
                articleId = jsonObject.getInt(KEY_ID);
                textViewTitle.setText(jsonObject.getString(KEY_TITLE));
                textViewShortDescription.setText(jsonObject.getString(KEY_SHORT_DESCRIPTION));
            } else if (layoutResult.getVisibility() != View.VISIBLE) {
                textViewNoResult.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean hasCameraPermission() {
        return ContextCompat.checkSelfPermission(this, CAMERA) == PERMISSION_GRANTED;
    }

    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    private void showProgressDialog() {
        progressDialog.setMessage("Loading result for " + mQrCode + "..");
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
