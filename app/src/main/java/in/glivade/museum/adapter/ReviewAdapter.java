package in.glivade.museum.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.glivade.museum.R;
import in.glivade.museum.model.Review;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ReviewHolder> {

    private List<Review> reviewList;
    private ReviewCallback callback;
    private int userId;

    public ReviewAdapter(List<Review> reviewList, ReviewCallback callback, int userId) {
        this.reviewList = reviewList;
        this.callback = callback;
        this.userId = userId;
    }

    @Override
    public ReviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReviewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_review, parent, false));
    }

    @Override
    public void onBindViewHolder(ReviewHolder holder, int position) {
        Review review = reviewList.get(position);
        holder.textViewName.setText(review.getName());
        holder.textViewDate.setText(review.getLastUpdated());
        holder.textViewRating.setText(String.valueOf(review.getRating()));
        holder.textViewReview.setText(review.getReview());
        holder.itemView.setBackgroundResource(holder.getLayoutPosition() % 2 == 0 ?
                android.R.color.transparent : R.color.grey_60);
        holder.imageViewEdit.setVisibility(review.getUserId() == userId
                ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    public interface ReviewCallback {
        void onEditClick(int position);
    }

    class ReviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewName, textViewDate, textViewRating, textViewReview;
        ImageView imageViewEdit;

        ReviewHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.txt_name);
            textViewDate = itemView.findViewById(R.id.txt_date);
            textViewRating = itemView.findViewById(R.id.txt_rating);
            textViewReview = itemView.findViewById(R.id.txt_review);
            imageViewEdit = itemView.findViewById(R.id.img_edit);
            imageViewEdit.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == imageViewEdit) callback.onEditClick(getLayoutPosition());
        }
    }
}
