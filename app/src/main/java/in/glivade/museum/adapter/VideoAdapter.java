package in.glivade.museum.adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.List;

import in.glivade.museum.R;
import in.glivade.museum.model.Video;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoHolder> {

    private List<Video> videoList;
    private VideoCallback callback;

    public VideoAdapter(List<Video> videoList, VideoCallback callback) {
        this.videoList = videoList;
        this.callback = callback;
    }

    @NonNull
    @Override
    public VideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VideoHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_video, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VideoHolder holder, int position) {
        final Video video = videoList.get(position);
        holder.thumbnailView.initialize(holder.itemView.getContext().getString(R.string.key_youtube),
                new YouTubeThumbnailView.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView,
                                                        final YouTubeThumbnailLoader youTubeThumbnailLoader) {
                        youTubeThumbnailLoader.setVideo(video.getCode());
                        youTubeThumbnailLoader.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                            @Override
                            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                                youTubeThumbnailLoader.release();
                            }

                            @Override
                            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

                            }
                        });
                    }

                    @Override
                    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView,
                                                        YouTubeInitializationResult youTubeInitializationResult) {

                    }
                });
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public interface VideoCallback {
        void onVideoClick(String code);
    }

    class VideoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        YouTubeThumbnailView thumbnailView;

        VideoHolder(View itemView) {
            super(itemView);
            thumbnailView = itemView.findViewById(R.id.thumbnail);
            thumbnailView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == thumbnailView)
                callback.onVideoClick(videoList.get(getLayoutPosition()).getCode());
        }
    }
}
