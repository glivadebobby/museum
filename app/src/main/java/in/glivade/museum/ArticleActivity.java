package in.glivade.museum;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.glivade.museum.adapter.ImageAdapter;
import in.glivade.museum.adapter.ReviewAdapter;
import in.glivade.museum.adapter.VideoAdapter;
import in.glivade.museum.app.AppController;
import in.glivade.museum.app.MyPreference;
import in.glivade.museum.model.Image;
import in.glivade.museum.model.Review;
import in.glivade.museum.model.Video;
import in.glivade.museum.util.Parser;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.glivade.museum.app.Api.KEY_ARTICLE_ID;
import static in.glivade.museum.app.Api.KEY_AUTHORS;
import static in.glivade.museum.app.Api.KEY_CODE;
import static in.glivade.museum.app.Api.KEY_DESCRIPTION;
import static in.glivade.museum.app.Api.KEY_ERROR;
import static in.glivade.museum.app.Api.KEY_ID;
import static in.glivade.museum.app.Api.KEY_IMAGES;
import static in.glivade.museum.app.Api.KEY_LAST_UPDATED;
import static in.glivade.museum.app.Api.KEY_MESSAGE;
import static in.glivade.museum.app.Api.KEY_MOBILE;
import static in.glivade.museum.app.Api.KEY_NAME;
import static in.glivade.museum.app.Api.KEY_RATING;
import static in.glivade.museum.app.Api.KEY_REVIEW;
import static in.glivade.museum.app.Api.KEY_REVIEWS;
import static in.glivade.museum.app.Api.KEY_TAG;
import static in.glivade.museum.app.Api.KEY_TITLE;
import static in.glivade.museum.app.Api.KEY_URL;
import static in.glivade.museum.app.Api.KEY_USER_ID;
import static in.glivade.museum.app.Api.KEY_VIDEOS;
import static in.glivade.museum.app.Api.URL;
import static in.glivade.museum.app.Api.VALUE_ARTICLE;
import static in.glivade.museum.app.MyActivity.launchWithBundle;

public class ArticleActivity extends AppCompatActivity implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener, Runnable, ReviewAdapter.ReviewCallback, VideoAdapter.VideoCallback, AdapterView.OnItemSelectedListener {

    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayout layoutArticleData;
    private TextView textViewTitle, textViewLastUpdated, textViewDescription, textViewAuthors,
            textViewNoImage, textViewNoVideo, textViewNoReview;
    private Spinner spinnerLang;
    private RecyclerView viewImages, viewVideos, viewReviews;
    private Button buttonWriteReview;
    private AnimationDrawable mAnimationDrawable;
    private List<Image> imageList;
    private List<Video> videoList;
    private List<Review> reviewList;
    private ImageAdapter imageAdapter;
    private VideoAdapter videoAdapter;
    private ReviewAdapter reviewAdapter;
    private MyPreference preference;
    private int articleId;
    private String description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) textViewDescription.setText(description);
        else {
            Toast.makeText(this, "Translating article..", Toast.LENGTH_SHORT).show();
            new TranslateText(this).execute(description, parent.getSelectedItem().toString());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View view) {
        if (view == buttonWriteReview) {
            Bundle bundle = new Bundle();
            bundle.putInt(KEY_ID, articleId);
            launchWithBundle(this, WriteReviewActivity.class, bundle);
        }
    }

    @Override
    public void onRefresh() {
        getArticle();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getArticle();
    }

    @Override
    public void onEditClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_REVIEW, reviewList.get(position));
        launchWithBundle(this, EditReviewActivity.class, bundle);
    }

    @Override
    public void onVideoClick(String code) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + code));
        intent.putExtra("force_fullscreen", true);
        startActivity(intent);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        layoutArticleData = findViewById(R.id.article_data);
        textViewTitle = findViewById(R.id.txt_title);
        textViewLastUpdated = findViewById(R.id.txt_last_updated);
        textViewDescription = findViewById(R.id.txt_description);
        textViewAuthors = findViewById(R.id.txt_authors);
        textViewNoImage = findViewById(R.id.txt_no_image);
        textViewNoVideo = findViewById(R.id.txt_no_video);
        textViewNoReview = findViewById(R.id.txt_no_review);
        spinnerLang = findViewById(R.id.spin_lang);
        viewImages = findViewById(R.id.images);
        viewVideos = findViewById(R.id.videos);
        viewReviews = findViewById(R.id.reviews);
        buttonWriteReview = findViewById(R.id.btn_write_review);

        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.article).getBackground();
        preference = new MyPreference(this);
        imageList = new ArrayList<>();
        videoList = new ArrayList<>();
        reviewList = new ArrayList<>();
        imageAdapter = new ImageAdapter(imageList);
        videoAdapter = new VideoAdapter(videoList, this);
        reviewAdapter = new ReviewAdapter(reviewList, this, preference.getId());
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        spinnerLang.setOnItemSelectedListener(this);
        buttonWriteReview.setOnClickListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            articleId = bundle.getInt(KEY_ID);
        }
    }

    private void initRecyclerView() {
        viewImages.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        viewImages.setAdapter(imageAdapter);

        viewVideos.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        viewVideos.setAdapter(videoAdapter);

        viewReviews.setLayoutManager(new LinearLayoutManager(this));
        viewReviews.setAdapter(reviewAdapter);
    }

    private void initRefresh() {
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.post(this);
    }

    private void getArticle() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        refreshLayout.setRefreshing(false);
                        handleArticleResult(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                refreshLayout.setRefreshing(false);
                Toast.makeText(ArticleActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_ARTICLE);
                params.put(KEY_ID, String.valueOf(articleId));
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "article");
    }

    private void handleArticleResult(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt(KEY_ERROR);

            if (error == 0) {
                imageList.clear();
                videoList.clear();
                reviewList.clear();

                layoutArticleData.setVisibility(View.VISIBLE);
                articleId = jsonObject.getInt(KEY_ID);
                textViewTitle.setText(jsonObject.getString(KEY_TITLE));
                textViewLastUpdated.setText(String.format(Locale.getDefault(),
                        getString(R.string.format_last_updated),
                        Parser.getRelativeTime(jsonObject.getString(KEY_LAST_UPDATED))));
                textViewDescription.setText(jsonObject.getString(KEY_DESCRIPTION));
                textViewAuthors.setText(String.format(Locale.getDefault(),
                        getString(R.string.format_authors),
                        jsonObject.getString(KEY_AUTHORS)));
                description = jsonObject.getString(KEY_DESCRIPTION);

                JSONArray imageArray = jsonObject.getJSONArray(KEY_IMAGES);
                for (int i = 0; i < imageArray.length(); i++) {
                    JSONObject object = imageArray.getJSONObject(i);
                    imageList.add(new Image(object.getInt(KEY_ID), object.getInt(KEY_ARTICLE_ID),
                            object.getString(KEY_URL)));
                }
                imageAdapter.notifyDataSetChanged();

                JSONArray videoArray = jsonObject.getJSONArray(KEY_VIDEOS);
                for (int i = 0; i < videoArray.length(); i++) {
                    JSONObject object = videoArray.getJSONObject(i);
                    videoList.add(new Video(object.getInt(KEY_ID), object.getInt(KEY_ARTICLE_ID),
                            object.getString(KEY_CODE)));
                }
                videoAdapter.notifyDataSetChanged();

                boolean hasReviewed = false;
                JSONArray reviewArray = jsonObject.getJSONArray(KEY_REVIEWS);
                for (int i = 0; i < reviewArray.length(); i++) {
                    JSONObject object = reviewArray.getJSONObject(i);
                    int userId = object.getInt(KEY_USER_ID);
                    if (!hasReviewed) hasReviewed = preference.getId() == userId;
                    reviewList.add(new Review(object.getInt(KEY_ID), object.getInt(KEY_ARTICLE_ID),
                            userId, Float.parseFloat(object.getString(KEY_RATING)),
                            object.getString(KEY_REVIEW), object.getString(KEY_NAME),
                            object.getString(KEY_MOBILE),
                            Parser.getRelativeTime(object.getString(KEY_LAST_UPDATED))));
                }
                reviewAdapter.notifyDataSetChanged();

                textViewNoImage.setVisibility(imageList.isEmpty() ? View.VISIBLE : View.GONE);
                textViewNoVideo.setVisibility(videoList.isEmpty() ? View.VISIBLE : View.GONE);
                textViewNoReview.setVisibility(reviewList.isEmpty() ? View.VISIBLE : View.GONE);

                buttonWriteReview.setVisibility(hasReviewed ? View.GONE : View.VISIBLE);
            } else {
                String message = jsonObject.getString(KEY_MESSAGE);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static class TranslateText extends AsyncTask<String, Void, String> {

        private final WeakReference<ArticleActivity> weakReference;

        public TranslateText(ArticleActivity activity) {
            this.weakReference = new WeakReference<>(activity);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            Translate translate = TranslateOptions.getDefaultInstance().getService();
            Translation translation = translate.translate(strings[0],
                    Translate.TranslateOption.sourceLanguage("en"),
                    Translate.TranslateOption.targetLanguage(strings[1]));
            return translation.getTranslatedText();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (weakReference.get() != null) {
                weakReference.get().textViewDescription.setText(s);
            }
        }
    }
}
