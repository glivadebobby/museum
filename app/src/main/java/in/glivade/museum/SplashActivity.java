package in.glivade.museum;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import in.glivade.museum.app.MyPreference;

import static in.glivade.museum.app.MyActivity.launch;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyPreference preference = new MyPreference(this);
        if (preference.getId() == -1) launch(this, LoginActivity.class);
        else launch(this, MainActivity.class);
    }
}
