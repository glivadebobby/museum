package in.glivade.museum.util;


import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Parser {
    public static String getRelativeTime(String dateTime) {
        try {
            Date parsedTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.getDefault()).parse(dateTime);
            return DateUtils.getRelativeTimeSpanString(parsedTime.getTime(), System.currentTimeMillis(),
                    DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;
    }
}
